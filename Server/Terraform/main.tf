terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.95.0"
    }
  }
  required_version = ">=1.5.3"
}

provider "yandex" {
  token     = "${var.token}"
  cloud_id  = "${var.cloud_id}"
  folder_id = "${var.folder_id}"
}

# Создание и настройка сервера

resource "yandex_compute_image" "os" {
  name = "ubuntu"
  source_image = "fd8nru7hnggqhs9mkqps"
}

resource "yandex_compute_instance" "vm_1" {
  name        = "vm-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = yandex_compute_image.os.id
      type     = "network-hdd"
      size     = 15
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet_servers_1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

# Создание и настройка сетей

resource "yandex_vpc_network" "network_servers_1" {
  name = "network-servers-1"
}

resource "yandex_vpc_subnet" "subnet_servers_1" {
  name           = "subnet-servers-1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_servers_1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

# Создание группы 

resource "yandex_lb_target_group" "target_group_1" {
  name      = "target-group-1"
  region_id = "ru-central1"

  target {
    subnet_id = "${yandex_vpc_subnet.subnet_servers_1.id}"
    address   = "${yandex_compute_instance.vm_1.network_interface.0.ip_address}"
  }

}

# Создание и настройка балансировщика 

resource "yandex_lb_network_load_balancer" "load_balancer_1" {
  name = "load-balancer-1"

  listener {
    name = "listener-1"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.target_group_1.id}"

    healthcheck {
      name                = "health-check"
      unhealthy_threshold = 5
      healthy_threshold   = 5
      http_options {
        port = 80
      }
    }
  }
}

# DNS

resource "yandex_dns_zone" "zone_1" {
  name        = "zone-1"
  description = "Public zone"
  zone        = "my-site-top.ru."
  public      = true
  
}

resource "yandex_dns_recordset" "rs-1" {
  zone_id = yandex_dns_zone.zone_1.id
  name    = "my-site-top.ru."
  ttl     = 600
  type    = "A"
  data    = ["${[for s in yandex_lb_network_load_balancer.load_balancer_1.listener: s.external_address_spec.*.address].0[0]}"]
}

resource "yandex_dns_recordset" "rs-2" {
  zone_id = yandex_dns_zone.zone_1.id
  name    = "www"
  ttl     = 600
  type    = "CNAME"
  data    = ["my-site-top.ru"]
}

# Создание файла hosts

resource "local_file" "ansible_inventory" {
  content = templatefile("./hosts",
    {
      host_1 = yandex_compute_instance.vm_1.name
      address_1 = yandex_compute_instance.vm_1.network_interface.0.nat_ip_address
    }
  )
  filename = "../Ansible/hosts"
}

# Вывод адресов

output "IP-сервера" {
  value = yandex_compute_instance.vm_1.network_interface.0.nat_ip_address
}

output "IP-балансировщика" {
  value = "${[for s in yandex_lb_network_load_balancer.load_balancer_1.listener: s.external_address_spec.*.address].0[0]}"
}