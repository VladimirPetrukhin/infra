# infra
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Как выполнить создание инфраструктуры в Yandex cloud:  
1. Перейти в папку ```Terraform``` и открыть файл ```terraform.tfvars```
2. Указать в файле необходимые данные для подключения к аккаунту ```yandex cloud```\
   ```token``` - Токен для подключения\
   ```cloud_id``` - Идентификатор облака\
   ```folder_id``` - Каталог
3. Открыть терминал ```shell```
4. Перейти в папку ```Terraform```:
   ```shell
   cd infra/Terraform
   ```
5. Выполнить команду:
   ```shell
   terraform init && terraform apply 
   ```

## Как выполнить установку компонентов Docker на сервер
1. Открыть терминал ```shell```
2. Перейти в папку ```Ansible```:
   ```
   cd infra/Ansible
   ```
3. Выполнить команду:
   ```
   ansible-playbook -b play.yml -v
   ```

Информация об авторе
------------------

- Электронная почта: <petrukhin.ru@yandex.ru>
