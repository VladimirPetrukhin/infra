variable "token" {
  description = "Токен для подключения"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "Идентификатор облака для подключения"
  type        = string
  sensitive   = true
}

variable "folder_id" {
  description = "Каталог для подключения"
  type        = string
  sensitive   = true
}
