terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.95.0"
    }
  }
  required_version = ">=1.5.3"
}

provider "yandex" {
  token     = "${var.token}"
  cloud_id  = "${var.cloud_id}"
  folder_id = "${var.folder_id}"
}

# Создание и настройка раннера

resource "yandex_compute_instance" "runner_1" {
  name        = "runner-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = "fd8nru7hnggqhs9mkqps"
      type     = "network-hdd"
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_runners_1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

# Создание и настройка сетей

resource "yandex_vpc_network" "network_runners_1" {
  name = "network-runners-1"
}

resource "yandex_vpc_subnet" "subnet_runners_1" {
  name           = "subnet-runners_1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_runners_1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

# Создание файла hosts

resource "local_file" "ansible_inventory" {
  content = templatefile("./hosts",
    {
      host_1 = yandex_compute_instance.runner_1.name
      address_1 = yandex_compute_instance.runner_1.network_interface.0.nat_ip_address
    }
  )
  filename = "../Ansible/hosts"
}


# Вывод адресов

output "IP-раннера" {
  value = yandex_compute_instance.runner_1.network_interface.0.nat_ip_address
}