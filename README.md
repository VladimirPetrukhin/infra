# infra
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Описание
Проект включает в себя создание и настройку:
1. Cервера для приложения (Папка `Server`) 
2. Сервера раннера, для выполнения Pipeline  (Папка `Runner`)

## Требования
1. Создание сервера осуществляется при помощи `Terraform` в `Yandex cloud`
2. Необходимая настройка машин выполняется при помощи `Ansible` 

Информация об авторе
------------------

- Электронная почта: <petrukhin.ru@yandex.ru>